Dwarven Halls w/ Ceilings
 
Name: CTP Dwarven Halls w/ Ceilings 
Designer: JDA, Pstemarie
Primary Classification: Interior
Subclassification(s): Caves
Filename: dh.hak 
Contains Additional Content? [Yes/No]: Yes (alternate texture version dh_granite.hak)

Credited Contributions: JDA Dwarven Halls, based on dhfin3.zip from the vault, which is supposed to be the last work that JDA will do on this set.

Additional credits notes:

1) This set would not have been fixed without the source files provided by Gribo. Thanks Gribo, your help with that, really saved us a lot of work on fixing those non-solid walls!

2) An extra special thank you to OldMansBeard who was able to create a special tool to help us fix some pretty major problems, all at once! That saved us untold hours of human effort! Thank you OMB!

Stilgar (ITP Fixing), Merc Artax (Walkmesh fixing, Tile slicing), Gribo (For creating the Cavern Entrance group), the EOB team for massive testing and help in squishing bugs. This update was built by Gribo, who fixed all lighting issues occurring in the Hak Pak; e.g. the differently lightened floors (you had some darker parts at the edges) increasing the visual correctness of the Dwarven Halls a lot. Many thanks for getting this done finally go to Gribo and the ALFA team.
This does NOT include the work done by CTP to actually get this set useable.

Additional thanks go to RedOne who helped us get tilefade working correctly with this set.

Summary/Critique/Tips/Notes:

This is one beautiful set, very good attention to detail. Complete with pits, cave entrances, internal rooms with mosaics. Some nice additional content buried in here. This set could be used as a Dwarven Hall as the name suggests, but could also be used to represent a mechanic type environment. A workshop of mechanically inclined gnomes for example. For those of you familiar with the EIDOS game series 'Thief' this set would work very well as a base for a mechanical age with the right dim lighting. 